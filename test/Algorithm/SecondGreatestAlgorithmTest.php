<?php

namespace Test\Algorithm;

use PHPUnit\Framework\TestCase;
use App\Algorithm\SecondGreatestAlgorithm;

/**
 * Class GreatestAlgorithmTest
 *
 * @package Test\Algorithm
 */
class SecondGreatestAlgorithmTest extends TestCase
{
    public function testConstructor()
    {
        $this->expectNotToPerformAssertions();

        new SecondGreatestAlgorithm([1, 2, 3, 4, 5, 6]);
    }

    /**
     * Note: for more readabilty method signature should be in snake_case
     */

    public function test_it_can_find_second_greatest_in_array(): void
    {
        $this->assertEquals(5, (new SecondGreatestAlgorithm([6, 2, 1, 4, 3, 5]))->find());
        $this->assertEquals(6, (new SecondGreatestAlgorithm([1, 5, 3, 4, 7, 6, 2]))->find());
        $this->assertEquals(7, (new SecondGreatestAlgorithm([5, 3, 2, 4, 6, 8, 7, 1]))->find());
        $this->assertEquals(8, (new SecondGreatestAlgorithm([8, 1, 9, 2, 5, 3, 4, 6, 7]))->find());
    }
}
