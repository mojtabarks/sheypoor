<?php

namespace Test\Algorithm;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use App\Algorithm\NthGreatestByQuickSort;

class NthGreatestByQuickSortTest extends TestCase
{
    public function test_it_should_throw_exception_if_nth_greater_than_array_length(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('nth must be less than or equal to the length of the array');

        (new NthGreatestByQuickSort([6, 2, 1, 4, 3, 5]))->find(10);
    }

    public function test_it_should_throw_exception_if_nth_is_not_integer(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('nth must be an integer');

        (new NthGreatestByQuickSort([6, 2, 1, 4, 3, 5]))->find('2');
    }

    public function test_it_should_throw_exception_if_nth_is_negative(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('nth must be greater than');

        (new NthGreatestByQuickSort([6, 2, 1, 4, 3, 5]))->find(-2);
    }

    public function test_it_can_find_nth_greatest_element_in_array(): void
    {
        $this->assertEquals(5, (new NthGreatestByQuickSort([6, 2, 1, 4, 3, 5]))->find(2));
        $this->assertEquals(5, (new NthGreatestByQuickSort([1, 5, 3, 4, 7, 6, 2]))->find(3));
        $this->assertEquals(5, (new NthGreatestByQuickSort([5, 3, 2, 4, 6, 8, 7, 1]))->find(4));
        $this->assertEquals(5, (new NthGreatestByQuickSort([8, 1, 9, 2, 5, 3, 4, 6, 7]))->find(5));
    }
}