<?php

namespace App\Traits;

use InvalidArgumentException;

trait Validation
{
    /**
     * @param int $nth
     * @param int $count
     * @return self
     * 
     * @throws InvalidArgumentException
     */
    public function nthShouldBeLessThanCount(int $nth, int $count): self
    {
        if ($nth > $count) {
            throw new InvalidArgumentException(
                sprintf('nth must be less than or equal to the length of the array, but nth: %d, count: %d', $nth, $count)
            );
        }
        return $this;
    }

    /**
     * @param mixed $nth
     * @return self
     * 
     * @throws InvalidArgumentException
     */
    public function nthShouldBeInteger($nth): self
    {
        if (!is_int($nth)) {
            throw new InvalidArgumentException('nth must be an integer');
        }
        return $this;
    }

    /**
     * @param int $nth
     * @return self
     * 
     * @throws InvalidArgumentException
     */
    public function nthShouldBeGreaterThanZero(int $nth): self
    {
        if ($nth < 1) {
            throw new InvalidArgumentException('nth must be greater than 0');
        }
        return $this;
    }
}