<?php

namespace App\Algorithm;

use App\Contracts\Algorithm;
use App\Contracts\FindInterface;

/**
 * Class NthGreatestByQuickSort
 * 
 * This class finds the nth greatest element of an arbitrarily
 * unordered array of elements.
 * 
 * @package App\Algorithm
 */
class NthGreatestByQuickSort extends Algorithm implements FindInterface
{
    /**
     * Finds the second greatest element of the data By Quick Sort.
     * this function uses of O(n) time complexity and O(1) space complexity.
     *
     * @param int|mixed $nth
     * @return mixed
     */
    public function find($nth = 2)
    {
        $this->nthShouldBeInteger($nth)
            ->nthShouldBeGreaterThanZero($nth)
            ->nthShouldBeLessThanCount($nth, count($this->data));

        $this->quickSort($this->data, 0, count($this->data) - 1);

        return $this->data[count($this->data) - $nth];
    }

    /**
     * Quick Sort Algorithm.
     *
     * @param array $data
     * @param int $low
     * @param int $high
     */
    private function quickSort(array &$data, int $low, int $high)
    {
        if ($low < $high) {
            $pivot = $this->partition($data, $low, $high);
            $this->quickSort($data, $low, $pivot - 1);
            $this->quickSort($data, $pivot + 1, $high);
        }
    }

    /**
     * Partition Algorithm.
     *
     * @param array $data
     * @param int $low
     * @param int $high
     * @return int
     */
    private function partition(array &$data, int $low, int $high): int
    {
        $pivot = $data[$high];
        $i = $low - 1;

        for ($j = $low; $j < $high; $j++) {
            if ($data[$j] <= $pivot) {
                $i++;
                $this->swap($data, $i, $j);
            }
        }

        $this->swap($data, $i + 1, $high);

        return $i + 1;
    }

    /**
     * Swap Algorithm.
     *
     * @param array $data
     * @param int $i
     * @param int $j
     */
    private function swap(array &$data, int $i, int $j)
    {
        $temp = $data[$i];
        $data[$i] = $data[$j];
        $data[$j] = $temp;
    }
}