<?php

namespace App\Algorithm;

use App\Contracts\Algorithm;
use App\Contracts\FindInterface;

/**
 * Class SecondGreatestAlgorithm
 *
 * This class finds the second greatest element of an arbitrarily
 * ordered array of elements.
 *
 * @package App\Algorithm
 */
class SecondGreatestAlgorithm extends Algorithm implements FindInterface
{
    /**
     * Finds the second greatest element of the data.
     * this function uses of O(n) time complexity and O(1) space complexity.
     * 
     * Note : in this function we don't use nth parameter.
     *
     * @return mixed
     */
    public function find($nth = 2)
    {
        $greatest = 0;
        $secondGreatest = 0;

        foreach ($this->data as $number) {
            if ($number > $greatest) {
                $secondGreatest = $greatest;
                $greatest = $number;
            } elseif ($number > $secondGreatest) {
                $secondGreatest = $number;
            }
        }

        return $secondGreatest;
    }
}
