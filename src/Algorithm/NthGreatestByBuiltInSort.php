<?php

namespace App\Algorithm;

use App\Contracts\Algorithm;
use App\Contracts\FindInterface;

/**
 * Class SecondGreatestAlgorithm
 * 
 * This class finds the nth greatest element of an arbitrarily
 * unordered array of elements.
 * 
 * @package App\Algorithm
 */
class NthGreatestByBuiltInSort extends Algorithm implements FindInterface
{
    /**
     * Finds the nth greatest element of the data by built-in sort php function.
     * this function uses of O(n log n) time complexity and O(1) space complexity.
     * the big-O of [count] is O(1)
     * 
     * @param int|mixed $nth
     * @return mixed
     */
    public function find($nth = 2)
    {
        $this->nthShouldBeInteger($nth)
            ->nthShouldBeGreaterThanZero($nth)
            ->nthShouldBeLessThanCount($nth, count($this->data));

        sort($this->data);

        return $this->data[count($this->data) - $nth];
    }
}