<?php

namespace App\Contracts;

interface FindInterface
{
    /**
     * @param int|mixed $nth
     * @return mixed
     */
    public function find($nth = 2);
}