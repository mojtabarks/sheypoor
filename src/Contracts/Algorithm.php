<?php

namespace App\Contracts;

use App\Traits\Validation;

abstract class Algorithm
{
    use Validation;

    /** @var array */
    protected array $data;

    /**
     * Algorithm constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
}