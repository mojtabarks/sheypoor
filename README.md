# sheypoor interview

## installation

run the following commands
````
$ git clone https://gitlab.com/mojtabarks/sheypoor.git
$ cd {PROJECT_FOLDER}
$ composer i
````

### how to make sure the code is working?

run the following command:
````
$ ./vendor/bin/phpunit
````

### how much is the code coverage?
run the following command:
````
$ ./vendor/bin/phpunit --coverage-html test/coverage
````

go to {PROJECT_FOLDER} -> test -> coverage

open `index.html` file
